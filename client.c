#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>
#include <sys/msg.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"

/*Handler de signaux*/
void handle_sigint(int sig) {
	if (sig == 2){
		exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]){
	/*Attacher un handler de signaux après avoir cree les ressouces IPC necessaires*/
	signal(SIGINT, handle_sigint);
	char* ptr;
	int msgid;

	srand(getpid());

	couleur(ROUGE);
	printf("Un nouveau client arrive !\n");
	couleur(BLANC);

//Récupere le nombre de chef, il y'a autant de chef que de file de message entre chef et client
  int nbr_chef_atelier = (int) strtol(argv[1],NULL,10);

  //Crée un tableau qui contiendra toutes les clés IPC
  key_t *tableau_cle = (key_t*) calloc(nbr_chef_atelier,sizeof(key_t));

  //Crée un tableau qui contiendra tous les id des msgQ
  int *tableau_id_msgQ = (int*) calloc(nbr_chef_atelier,sizeof(int));

	//Crée un tableau qui contiendra le nombre de msg pour chaque file chef
	int *tableau_nbr_msg_msgQ_chef = (int*) calloc(nbr_chef_atelier,sizeof(int));

  int i=0;
	struct msqid_ds bufferInfoMsq;
  for(i=0;i<nbr_chef_atelier;i++){//Parcours les clés en parametres
    tableau_cle[i] = (key_t) strtol(argv[i+2],NULL,10); //Récupere la clé
    tableau_id_msgQ[i] = (int) msgget(tableau_cle[i],0); //Récupere l'id lié à la clé
		msgctl(tableau_id_msgQ[i],IPC_STAT,&bufferInfoMsq);//Récupere les infos de chaque msgQ
		tableau_nbr_msg_msgQ_chef[i]=bufferInfoMsq.msg_qnum;//Récupere le nombre de message de chaque msgQ
  }

	int bufferMin=tableau_nbr_msg_msgQ_chef[0],rank=0,tailleTableau = nbr_chef_atelier;
	for(i=0;i<tailleTableau;i++){//Trouve la queue avec le moins de messages
		if(tableau_nbr_msg_msgQ_chef[i]<bufferMin){
			bufferMin=tableau_nbr_msg_msgQ_chef[i];
			rank=i;
		}
	}

	int taillePayloadTravail = sizeof(int)*6+sizeof(size_t);
	travail_t requete;
	requete.type = rank;//Type = le numero d'ordre du chef
    requete.pidClient = getpid();
	requete.choix = rand()%NB_REPARATION;//Choisis une réparation au hasard
	msgsnd(tableau_id_msgQ[rank],&requete,taillePayloadTravail,0);//Envoie une requete au chef le moins occupé

	travail_t buffReponse;
	while(1){
		sleep(1);//Attente arbitraire
		if(msgrcv(tableau_id_msgQ[rank],&buffReponse,taillePayloadTravail, getpid(), 0)) break;//Récupere la reponse du serveur et quitte la boucle
	}

	return 0;
}
