#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>
#include <sys/msg.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"

/*Handler de signaux*/
void handle_sigint(int sig) {
	if (sig == 2){
		exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]){
	char* ptr;
	int msgid;

	srand(getpid());

	int nb_ordre = atoi(argv[1]);
    int nb_outils_cat1 = atoi(argv[2]);
	int nb_outils_cat2 = atoi(argv[3]);
	int nb_outils_cat3 = atoi(argv[4]);
	int nb_outils_cat4 = atoi(argv[5]);

	couleur(JAUNE);
	printf("Le serveur %d (%d) peut se mettre au travail\n", nb_ordre, (int) getpid());
	couleur(BLANC);

	key_t key = ftok("cle.serv", nb_ordre);
	
	if((msgid = msgget(key, IPC_CREAT|(0600))) == -1){
		perror("msgget serveur");
		exit(EXIT_FAILURE);
	}

	/*Attacher un handler de signaux après avoir cree les ressouces IPC necessaires*/
	signal(SIGINT, handle_sigint);

	/*Definition des types des struct*/
	travail_t travail;
	travail.type = nb_ordre;

    reponse_meca rep_meca;

	while(1){

		/*Le chef d'atelier recoit une requête*/
		if(msgrcv(msgid, &travail, sizeof(travail_t) - sizeof(long), 1, 0) == -1) {
			perror("Erreur lors de la reception d'une requete (chef d'atelier)");
			exit(EXIT_FAILURE);
		} else {
			couleur(JAUNE);
			printf("Le chef d'atelier %d (%d) a recu une requête, la réparation est la n°%d \n", nb_ordre, (int) getpid(), travail.choix);
			couleur(BLANC);
		}

		/*Le chef d'atelier se deplace jusqu'à l'atelier !*/
		sleep(rand()%2);

        /*Le chef d'atelier défini le nombre d'outils disponibles*/
        /*Valeur prises au hasard*/
        travail.nb_outils_mecanicien[0] = rand()%nb_outils_cat1;
        travail.nb_outils_mecanicien[1] = rand()%nb_outils_cat2;
        travail.nb_outils_mecanicien[2] = rand()%nb_outils_cat3;
        travail.nb_outils_mecanicien[3] = rand()%nb_outils_cat4;

        travail.duree = rand()%10 + 5;

		/*Le chef d'atelier envoie le travail à faire et le nombre d'outils requis
        pour faire le travail à un mécanicien*/
		if(msgsnd(msgid, &travail, sizeof(travail_t) - sizeof(long), 0) == -1) {
			perror("Erreur lors de l'envoi de la requete msg (chef d'atelier-mécanicien)");
			exit(EXIT_FAILURE);
		}

		/*La voiture est prête*/
		if(msgrcv(msgid, &rep_meca, sizeof(reponse_meca) - sizeof(long), 3, 0) == -1) {
			perror("Erreur lors de la reception d'une requete (depuis mécanicien)");
			exit(EXIT_FAILURE);
		}

		/*Le chef d'atelier se redirige vers l'accueil !*/
		sleep(rand()%2);

		/*Le chef d'atelier rend la voiture réparé au client correspondant*/
		if(msgsnd(msgid, &rep_meca, sizeof(reponse_meca) - sizeof(long), 0) == -1) {
			perror("Erreur lors de l'envoi de la requete msg (chef d'atelier-mécanicien)");
			exit(EXIT_FAILURE);
		} else {
			couleur(JAUNE);
			printf("Le chef d'atelier %d (%d) vient de rendre la voiture n°%d\n", nb_ordre, (int) getpid(), travail.choix);
			couleur(BLANC);
		}

	}

	return 0;
}