#include <sys/types.h>

typedef struct
{
	long type;
	pid_t pidClient;
	int choix;
	int nb_outils_mecanicien[4];
	int duree;
} travail_t;

typedef struct
{
	long type;
	char c; // arbitraire juste pour faire un payload
} reponse_meca;

#define NBCLIENTMAX 4
#define NB_OUTILS 4
#define NB_REPARATION 5

#define couleur(param) fprintf(stdout,"\033[%sm",param)

#define NOIR "30"
#define ROUGE "31"
#define VERT "32"
#define JAUNE "33"
#define BLEU "34"
#define MAGENTA "35"
#define CYAN "36"
#define BLANC "37"
#define REINIT "0"
