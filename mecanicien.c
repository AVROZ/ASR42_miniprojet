#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>
#include <sys/msg.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include "main.h"

/*Handler de signaux*/
void handle_sigint(int sig){
	if (sig == 2){
		exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]){
	int msgid;
	int shmid;
	int shmid_nb_outils;

	srand(getpid());

	/*Numéro d'ordre du mécanicien*/
	int nb_ordre = atoi(argv[1]);

	couleur(CYAN);
	printf("Le mécanicien %d (%d) peut se mettre au travail\n", nb_ordre, (int) getpid());
	couleur(BLANC);

	int* outils_shm;

	/*Definition des cles qui seront utilisees*/
	key_t key = ftok("/tmp", 1);
	key_t key_chef_atelier = ftok("cle.serv", 'a');
	key_t key_mecanicien;

	if (-1 != open("/tmp/outils", O_CREAT, 0600)) {
		key_mecanicien = ftok("/tmp/outils", 0);
	} else {
		perror("open");
		exit(EXIT_FAILURE);
	}

	/*Definition des IPC*/
	shmid_nb_outils = shmget(key_mecanicien, 0, 0);

	shmid = shmget(key, 0, 0);

	if((msgid = msgget(key_chef_atelier, IPC_CREAT|(0600))) == -1){
		perror("msgget (mécanicien)");
		exit(EXIT_FAILURE);
	}

	outils_shm = (int *) shmat(shmid, NULL, 0);

	/*Definition des types des struct*/
	travail_t travail;
	travail.type = 2;

    reponse_meca rep_meca;

	int semid = semget(key, 1, 0600|IPC_CREAT|IPC_EXCL);

	/*Definition des semaphores*/
	struct sembuf p[NB_OUTILS], v[NB_OUTILS];

    int n = 0;
    for(int i = 0; i < NB_OUTILS; i++) {
        if (travail.nb_outils_mecanicien[i] > 0) {
            p[n].sem_num = i;
            p[n].sem_op = -travail.nb_outils_mecanicien[i];
            p[n].sem_flg = 0;
            v[n].sem_num = i;
            v[n].sem_op = travail.nb_outils_mecanicien[i];
            v[n].sem_flg = 0;
            n++;
        }
    }

	/*Attacher un handler de signaux après avoir cree les ressouces IPC necessaires*/
	signal(SIGINT, handle_sigint);
	
	/*Le mécanicien se prepare pour la journee*/
	sleep(2);

	while(1){

		/*Le mécanicien a recu une commande donnee par un chef d'atelier*/
		if(msgrcv(msgid, &travail, sizeof(travail_t) - sizeof(long), 2, 0) == -1) {
			perror("Erreur lors de la reception d'une requete msg (mécanicien)");
			exit(EXIT_FAILURE);
		} else {
			couleur(CYAN);
			printf("le mécanicien %d (%d) va choisir les outils pour la requête n %d\n", nb_ordre, (int) getpid(),travail.choix);
			couleur(BLANC);
		}

		/*Le mécanicien se rend sur l'établi*/
		sleep(rand()%3);

        /*Le mécanicien réserve les outils nécessaires*/
        semop(semid, p, n);

		couleur(CYAN);
		printf("le mécanicien %d (%d) répare la voiture n %d\n", nb_ordre, (int) getpid(),travail.choix);
		couleur(BLANC);

		sleep(travail.duree);

        /*Le mécanicien libère les outils nécessaires*/
        semop(semid, v, n);

		/*Le mécanicien notifie un serveur que la commande est prete*/
		if(msgsnd(msgid, &rep_meca, sizeof(reponse_meca) - sizeof(long), 0) == -1) {
			perror("Erreur lors de l'envoi de la requete msg (mecanicien-chef)");
			exit(EXIT_FAILURE);
		} else {
			couleur(CYAN);
			printf("Voiture n %d prete\n", travail.choix);
			couleur(BLANC);
		}
	}

	return 0;
}