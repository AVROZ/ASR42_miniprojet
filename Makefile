fail: 
	echo "Projet non terminé"

EXEC = main client chef_atelier mecanicien

client : client.o
    gcc -o client client.o

client.o : client.c
    gcc -c client.c

main : main.o
    gcc -o main main.o

main.o : main.c
    gcc -c main.c

chef_atelier : chef_atelier.o
    gcc -o chef_atelier chef_atelier.o

chef_atelier.o : chef_atelier.c
    gcc -c chef_atelier.c

mecanicien : mecanicien.o
    gcc -o mecanicien mecanicien.o

mecanicien.o : mecanicien.c
    gcc -c mecanicien.c


all : client main chef_atelier mecanicien


clean: 
    rm -f *.o
    rm -f $(EXEC)
    rm -f cle.*