#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/sem.h>
#include <signal.h>
#include <time.h>
#include <sys/msg.h>
#include <string.h>
#include <fcntl.h>
#include <wait.h>
#include "main.h"

int msgid;
int msgid_chef_atelier;
int shmid;
int shmid_mecanicien;
int semid;
key_t key;
key_t key_chef_atelier;
key_t key_mecanicien;

/*Permet de nettoyer les ressouces IPC avec un handler de signaux*/
void handle_sigint(int sig) {

	couleur(REINIT);

	if (sig == 2){
		printf("\nSignal %d : SIGINT recu ! : je termine le processus en nettoyant les ressources IPC !\n", sig);

		/*Supprime le jeu de semaphores*/

		if((msgctl(msgid, IPC_RMID, NULL)) == -1){
			perror("msgctl (main)");
			exit(EXIT_FAILURE);
		}

		if((msgctl(msgid_chef_atelier, IPC_RMID, NULL)) == -1){
			perror("msgctl (main)");
			exit(EXIT_FAILURE);
		} else {
			printf("Fermeture des files de message\n");
		}

		if((shmctl(shmid, IPC_RMID, 0)) == -1){
			perror("shmctl (main)");
			exit(EXIT_FAILURE);
		} else {
			printf("Fermeture des segments de mémoire partagée\n");
		}

		if((shmctl(shmid_mecanicien, IPC_RMID, 0)) == -1){
			perror("shmctl (main) 2");
			exit(EXIT_FAILURE);
		} else {
			printf("Fermeture des segments de mémoire partagée (2)\n");
		}
		
		if((semctl(semid, 0, IPC_RMID)) == -1){
			perror("semctl");
			exit(EXIT_FAILURE);

		} else {
			printf("Fermeture des semaphores\n");
		}
		
		exit(EXIT_SUCCESS);
	}
}

/*Lance un nouveau mecanicien*/
void lancer_nouveau_mecanicien(int num_ordre){
	char buf[256];
	pid_t p = fork();

	if (p == 0) {
		snprintf(buf, sizeof(buf), "%d", num_ordre);

		if((execl("./mecanicien", "./mecanicien", buf, NULL)) == -1){
			perror("execl");
			exit(EXIT_FAILURE);
		}

	} else if (p == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
}

/*Lance un nouveau chef_atelier*/
void lancer_nouveau_chef_atelier(int num_ordre, int nb_outils_cat1, int nb_outils_cat2, int nb_outils_cat3, int nb_outils_cat4){
	char buf[256];
	pid_t p = fork();

	if (p == 0) {
		snprintf(buf, sizeof(buf), "%d %d %d %d %d", num_ordre, nb_outils_cat1, nb_outils_cat2, nb_outils_cat3, nb_outils_cat4);

		if((execl("./chef_atelier", "./chef_atelier", buf, NULL)) == -1){
			perror("execl");
			exit(EXIT_FAILURE);
		}

	} else if (p == -1) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
}

/*Lance un nouveau client*/
void lancer_nouveau_client(int nb_chefs_ateliers, key_t chef_atelier_key){
	char buf[256];
	pid_t p = fork();

	if (p == 0) {
		snprintf(buf, sizeof(buf), "%d %d", nb_chefs_ateliers, chef_atelier_key);

		if(execl("./client", "./client", buf, NULL) == -1){
			perror("fork du client");
			exit(EXIT_FAILURE);
		}

	} else if (p == -1) {
		exit(EXIT_FAILURE);
		perror("fork");

	}
}

int main(int argc,char *argv[]) {

	/*Verification nombre d'arguments*/
	if (argc <= 5) {
		fprintf(stderr, "Erreur : syntaxe => \"./main nb_chefs nb_mecaniciens  nb_1 nb_2 nb_3 nb_4\"\n");
		exit(EXIT_FAILURE);
	}

	/*Definition du nombre de chefs d'atelier, mecaniciens, ...*/
	int const nb_chefs = atoi(argv[1]),
	nb_mecaniciens = atoi(argv[2]);

	int i, j;

	/*Definition du nombre d'outils que l'utilisateur demande*/
	int nb_max_outils = 0;
	for (i = 0; i < NB_OUTILS; i++) {
		nb_max_outils += atoi(argv[i+3]);
	}

	/*Définition du nombre d'outils pour chaque catégorie*/
	int nb_outils_cat1 = atoi(argv[3]);
	int nb_outils_cat2 = atoi(argv[4]);
	int nb_outils_cat3 = atoi(argv[5]);
	int nb_outils_cat4 = atoi(argv[6]);

	srand(getpid());

	/*Converti un nom de fichier et un identificateur de projet en cle IPC*/
	if((key = ftok("/tmp", 1)) == -1){
		perror("ftok");
		exit(EXIT_FAILURE);
	}

	if (open("/tmp/outils", O_CREAT, 0600 != -1)) {
		key_mecanicien = ftok("/tmp/outils", 0);
	} else {
		perror("open");
		exit(EXIT_FAILURE);
	}

	/*Pour obtenir un identifiant de file de messages*/
	if((msgid = msgget(key, IPC_CREAT|0600)) == -1){
		perror("msgget");
		exit(EXIT_FAILURE);
	}

	if (open("cle.serv", O_CREAT, 0600 != -1)) {
		key_chef_atelier = ftok("cle.serv", 'a');
	} else {
		perror("open");
		exit(EXIT_FAILURE);
	}

	if((msgid_chef_atelier = msgget(key_chef_atelier, IPC_CREAT|(0600))) == -1){
		perror("msgget chef_atelier");
		exit(EXIT_FAILURE);
	}

	/*Allouer un segment de mémoire partagée*/
	if((shmid_mecanicien = (shmget(key_mecanicien, sizeof(int), IPC_CREAT|0600))) == -1){
		perror("shmget");
		exit(EXIT_FAILURE);
	}

    /*Sémaphore*/
    if ((semid = semget(key, NB_OUTILS, 0600|IPC_CREAT|IPC_EXCL)) == -1) {
        perror("semget");
    }

	int* nb_outils_mecanicien;
	nb_outils_mecanicien = (int *) shmat(shmid_mecanicien, NULL, 0);

	/*Mettre en memoire partagée les données necessaires*/
	nb_outils_mecanicien[0] = nb_outils_cat1;
    nb_outils_mecanicien[1] = nb_outils_cat2;
    nb_outils_mecanicien[2] = nb_outils_cat3;
    nb_outils_mecanicien[3] = nb_outils_cat4;

    /*Attribution du nombre d'outils de chaque catégorie dans les sémaphores*/
    for(i = 0; i < NB_OUTILS; i++) {
        semctl(semid, i, SETVAL, nb_outils_mecanicien[i]);
    }

	/*Attacher un handler de signaux après avoir cree les ressouces IPC necessaires*/
	signal(SIGINT, handle_sigint);

	/*Lancement des chef_ateliers*/
	for (i = 0; i < nb_chefs; i++) {
		lancer_nouveau_chef_atelier(i, nb_outils_cat1, nb_outils_cat2, nb_outils_cat3, nb_outils_cat4);
	}

	/*Lancement des mecaniciens*/
	for (i = 0; i < nb_mecaniciens; i++){
		lancer_nouveau_mecanicien(i);
	}

	/*Lancement de $(NBCLIENTMAX) clients*/
	for (i = 0; i < NBCLIENTMAX; i++){
		lancer_nouveau_client(nb_chefs, key_chef_atelier);
	}

	/*Boucle qui permet de gerer les clients*/
	while(1){
		lancer_nouveau_client(nb_chefs, key_chef_atelier);
		wait(NULL);

		sleep(rand()%10);
	}

	exit(EXIT_SUCCESS);
}